#include "json/json.h"
#include <algorithm>
#include <stdio.h>

static std::string read_file(const char *path)
{
	FILE *file = fopen(path, "rb");
	if ( !file )
		return std::string("");

	fseek(file, 0, SEEK_END);
	long size = ftell(file);
	fseek(file, 0, SEEK_SET);
	std::string text;
	char *buffer = new char[size+1];
	buffer[size] = 0;

	if (fread(buffer, 1, size, file) == (unsigned long)size)
		text = buffer;

	fclose(file);
	delete[] buffer;
	return text;
}


static int
parse_value(const std::string &input)
{
	Json::Value root;
	const Json::Features features;
	Json::Reader reader(features);
	if (!reader.parse(input, root))
	{
		std::cout << "Failed to parse" << std::endl;
		return 1;
	}

	std::cout << "ip:" << root["ip"].asString() << std::endl;
	std::cout << "post:" << root["port"].asString() << std::endl;
	std::cout << "operDir:" << root["operDir"].asString() << std::endl;
	Json::Value interface = root["url"];
	Json::FastWriter fastWriter;
	for (unsigned int i = 0; i < interface.size(); i++)
	{
		std::cout << "###############" << std::endl;
		std::cout << "name:" << interface[i]["name"].asString() << std::endl;
			std::cout << "method:" << interface[i]["method"].asString() << std::endl;
		Json::Value post_data = interface[i]["post_data"];
	    std::cout << "post_data" << post_data << std::endl;
		
		std::string output = fastWriter.write(post_data);
		std::cout << "output" << output << std::endl;
	}
   return 0;
}



int main( int argc, const char *argv[] )
{
	int ret = 0;
	std::string input = read_file("test1.json");
	if (input.empty())
	{
		printf("Failed to read input or empty\n");
		return -1;
	}
	
	ret = parse_value(input);
	return ret;
}

